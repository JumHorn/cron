# restart openshift pods
curl -X PATCH -H "Authorization: Bearer $openshift" -H "Content-Type: application/json-patch+json" -k $openshift_api/apis/apps/v1/namespaces/jumhorn-dev/deployments/web/scale --data '[{"op": "replace", "path": "/spec/replicas", "value": 0}]'

sleep 20 # stop for 20 seconds

curl -X PATCH -H "Authorization: Bearer $openshift" -H "Content-Type: application/json-patch+json" -k $openshift_api/apis/apps/v1/namespaces/jumhorn-dev/deployments/web/scale --data '[{"op": "replace", "path": "/spec/replicas", "value": 1}]'
