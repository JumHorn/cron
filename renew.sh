# mysql
curl https://jumhorn.com/login

# mongodb
curl https://jumhorn.com/todo/

# redis
curl https://jumhorn.com/doodles

# segfault
echo ""
echo "prepare for segfault login:"
echo $segfault_secret
echo $segfault
cat $segfault
cp $segfault ./segfault
chmod 600 ./segfault
## use echo to send "enter" to pass press enter to continue
# echo | ssh -o "SetEnv SECRET=$segfault_secret" -i ./segfault root@8lgm.segfault.net "whoami;uptime;free -h"
## due to miss file ~/.ssh/know_hosts,Temporarily ignore the host key verification:
printf '\n\n\n' | ssh -v -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o "SetEnv SECRET=$segfault_secret" -i ./segfault root@8lgm.segfault.net "whoami;uptime;free -h"
